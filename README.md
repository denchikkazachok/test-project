**1. Опишите, какие проблемы могут возникнуть при использовании данного кода**

```
1. $mysqli = new mysqli("localhost", "my_user", "my_password", "world");
2. $id = $_GET['id'];
3. $res = $mysqli->query('SELECT * FROM users WHERE u_id='. $id);
4. $user = $res->fetch_assoc();
```

Ответы:

- [x] 1.1 'my_user' и 'my_password' следует использовать более защищенные (разные символы, регистры, длина ),  в таком примере можно просто сбрутить (+полезно использовать ограничение определенному пользователю по IP адресу на стороне MySQL-сервера)
- [x] 1.2 использовать библиотеку PDO (она имеет возможность использовать именнованные параметры :id которые безопаснее и исключают sql инъекции)

- [x] 2.1 следует сделать проверку на существование параметра в get-запросе
- [x] 2.2 в случае отсутсвия установить значение по умолчанию, я бы так и сделал
- [x] 2.3 важно производить очистку от ненужных тегов например strip_tags 
- [x] 2.4 еще бы я дополнительно сделал приведение типа к int в случае если у нас u_id int, тем самым отбросили бы все символы которые бы попали после обработки strip_tags

- [x] 3.1 выбирать все поля довольно избыточно предполагаю можно ограничиться только необходимыми 'email, phone' 
- [x] 3.2 добавить пагинацию и лимит в запросе LIMIT 0,10. тем самым будут выбраны 10 записей начиная с 0.  


**2. Сделайтерефакторинг**
```
$dbh = new PDO(
    'mysql:dbname=test;host=localhost',
    'root',
    'root',
    [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
);
$catId = 6;
$sql = <<<SQL
SELECT u.id as user_id, name, gender, qu.id as question_id, qu.catalog_id FROM questions AS qu
LEFT JOIN users u on u.id = qu.user_id
WHERE qu.catalog_id={$catId}
SQL;

$sth = $dbh->prepare($sql);
$sth->execute();
$array = $sth->fetchAll(PDO::FETCH_ASSOC);
print_r($array);
//единственное что упустил это преобразование обратно в массив
```

**3. Напиши SQL-запрос**
```
SELECT u.name, u.phone, SUM(os.subtotal) as summ, SUM(os.subtotal) / COUNT(u.id), MAX(os.created) as average
FROM orders as os
         LEFT JOIN users u on u.id = os.user_id
GROUP BY u.name, u.phone
```

**4. Напиши SQL-запросы**

Написать запрос для вывода самой большой зарплаты в каждом департаменте
- [x] SELECT departamentId, MAX(Salary) FROM users GROUP BY departamentId;

Написать запрос для вывода списка сотрудников из 3-го департамента у которых ЗП больше 90000
- [x] SELECT * FROM users WHERE departamentId = 3 AND salary > 90000;

Написать запрос по созданию индексов для ускорения
- [x] create index users_departamentId_index on users (departamentId); индекс для departamentId
- [x] create index users_salary_index on users (salary); индекс для salary


**5. Сделайте рефакторинг кода для работы с API чужого сервиса**

было
```
function printOrderTotal(responseString) {
   var responseJSON = JSON.parse(responseString);
responseJSON.forEach(function(item, index){
      if (item.price = undefined) {
item.price = 0;
      }
orderSubtotal += item.price;
   });
console.log( 'Стоимостьзаказа: ' + total> 0? 'Бесплатно': total + ' руб.');
}
```

стало
```
function printOrderTotal(responseString) {
    const prices = responseString.map(item => item.price ?? 0);
    const orderSubtotal = prices.reduce(
      (accumulator, currentValue) => accumulator + currentValue, 0);
    console.log('Стоимостьзаказа: ' + orderSubtotal > 0 ? 'Бесплатно': orderSubtotal + ' руб.');
}
```
